﻿module Fuhler.AliveAgent 
open System
open System.Net
open JsonHandler
open NLog

let private logger = NLog.LogManager.GetCurrentClassLogger() in
let private infoLog content = logger.Info<string>(content);;
let private warnLog content = logger.Warn<string>(content);;

(* System.Timer.Timer *)
let private timer = new System.Timers.Timer();;

(* response *)
type AliveAgentResponse = {
    host:string;
    isAlive:bool;
    statusCode:int;
    responseTime:int
}

(* command *)
type AliveAgentCommand =
    Execute of Uri * AsyncReplyChannel<AliveAgentResponse>
    | Continue
    | Kill

(* ToString *)
let private resToString (x:AliveAgentResponse) =
    (sprintf "isAlive:%s statusCode:%d responseTime:%d" (x.isAlive.ToString()) x.statusCode x.responseTime)

let private postAliveCheckExecutor (target_addr:Uri) =
    let host = target_addr.Host;
    let noCachePolicy = new Cache.HttpRequestCachePolicy(Cache.HttpRequestCacheLevel.NoCacheNoStore) in
    HttpWebRequest.DefaultCachePolicy <- noCachePolicy;
    let hr = WebRequest.CreateHttp target_addr in
    hr.UserAgent <- "Fuhler Alive-Monitoring System";
    hr.Method <- "GET";
    hr.KeepAlive <- false
    hr.Headers.Add("Pragma", "no-cache");
    hr.Headers.Add("Cache-Control", "no-cache");
    let timer = Diagnostics.Stopwatch.StartNew() in
    try
        let response = hr.GetResponse() in
        let httpResponse = response :?> HttpWebResponse in
            infoLog ("thisRequestIsFromCache:" + httpResponse.IsFromCache.ToString());
            timer.Stop();
            let ms = timer.ElapsedMilliseconds in
            httpResponse.Close();
            let res = {host=host;isAlive=true; statusCode=(int)httpResponse.StatusCode;responseTime=(int)ms} in
            res |> resToString |> infoLog;
            res
    with 
        :? WebException as e ->
            timer.Stop();
            let ms = timer.ElapsedMilliseconds in
            logger.WarnException("exception occured, ", e);
            match e.Status with
            WebExceptionStatus.ProtocolError ->
                let badWebResponse = e.Response :?> HttpWebResponse in
                {host=host;isAlive=false;statusCode=(int)badWebResponse.StatusCode;responseTime=(int)ms}
            | _ -> {host=host;isAlive=false;statusCode=0;responseTime=(int)ms}
        | _ -> {host=host;isAlive=false;statusCode=0;responseTime=0}
    

(* Define alive-monitoring agent *)
let aliveAgent = 
    new MailboxProcessor<AliveAgentCommand> (
        fun x ->
            let rec loop () = async {
                infoLog "wait for message."
                let! msg = x.Receive();
                match msg with
                Kill -> 
                    Console.WriteLine "Exiting AliveAgent"
                    return ()
                | Continue -> 
                    return! loop()
                | Execute (uri, rep) ->
                    infoLog ("try request to " + uri.AbsoluteUri + ".")
                    postAliveCheckExecutor uri |> rep.Reply;
                    infoLog ("request finished")
                    return! loop()
                } 
            in
                loop ()
    )

let timeAgentManager f (config:Settings) =
    let uri = Uri(config.host) in
    timer.Stop();
    timer.Interval <- (float)config.duration;
    timer.Elapsed.Add(fun e -> 
        infoLog "timer kicked!";
        let res = aliveAgent.PostAndAsyncReply(fun x -> Execute(uri, x)) in
        Async.StartWithContinuations(
            res, 
            (fun x -> f x),
            (fun _ -> ()),
            fun _ -> ())
    );
    timer.Start()