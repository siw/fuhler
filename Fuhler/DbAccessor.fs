﻿module Fuhler.DbAccessor
open Raven.Database
open Raven.Client.Linq
open Raven.Client.Linq.Indexing

type healthCheckDao() = 
    let mutable host:string = ""
    let mutable status:int = 0
    let mutable responseTime:int = 0
    let mutable time:int = 0

    member this.Host 
        with get() = host
        and set v = host <- v

    member this.Status 
        with get() = status
        and set v = status <- v

    member this.ResponseTime 
        with get() = responseTime
        and set v = responseTime <- v

    member this.Time 
        with get() = time
        and set v = time <- v

let initializedSession = 
    let ravendb = new Raven.Client.Embedded.EmbeddableDocumentStore();
    ravendb.DataDirectory <- "database";
    ravendb.Initialize();

let useRavenDataBase f =
    let store = initializedSession in
        let mgmt = store.Initialize()
        use session = mgmt.OpenSession() in
            (f session);
;;

let storeHealthCheck (res:healthCheckDao) = 
     useRavenDataBase(
        fun session ->
            session.Store(res);
            session.SaveChanges();
        )
;;

let getHealthCheck () = 
    useRavenDataBase(
        fun session ->
            let q = //session.Advanced.LoadStartingWith<healthCheckDao>("healthCheckDaos") in
                session.Query<healthCheckDao>().Where(fun x -> x.ResponseTime > 0);
            Seq.cast<healthCheckDao> q
    )
;;

let cleanUpDb () = 
    useRavenDataBase(
        fun session ->
            session.Advanced.DocumentStore.DatabaseCommands.Delete("healthCheckDaos", null);
            session.SaveChanges();
        )
;;

