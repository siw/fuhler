﻿module Fuhler.JsonHandler
open System.IO
open Newtonsoft.Json
open Newtonsoft.Json.Linq
type Settings = {
    host:string;
    duration:int;
    threshold_up:int;
    threshold_down:int
    threshold_resp_time:int;
    }
    ;;

type RuntimeSettings = {
    port:int;
    location: string
}

let getString (path:string) = 
    use txtReader = new StreamReader(path) in
    txtReader.ReadToEnd()

let getRuntimeSettings path = 
    let str = getString path in
    let json = JObject.Parse str in
    try 
        { port = (int)json.["port"]; location = (string)json.["location"]}
    with e ->
        System.Console.WriteLine("falied to load " + path);
        { port = 12345; location = "" };

let getSettings path = 
    let str = getString path in
    let json = JObject.Parse str in
    try
        let targets = json.SelectTokens("target") in
        Seq.toList (targets.Children<JToken>()) |>
        List.map (fun s -> 
            {
                host = (string)s.["host"];
                duration = (int)s.["duration"];
                threshold_up = (int)s.["threshold_up"];
                threshold_down = (int)s.["threshold_down"];
                threshold_resp_time = (int)s.["threshold_resp_time"]
            })
    with e ->
        System.Console.WriteLine ("failed to load config file. please check format");
        []
    
