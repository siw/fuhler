﻿open Fuhler.AliveAgent
open Fuhler.JsonHandler
open Fuhler.WebServer

let fuhler_logo = @"
        ___           ___           ___           ___       ___           ___     
       /\  \         /\__\         /\__\         /\__\     /\  \         /\  \    
      /::\  \       /:/  /        /:/  /        /:/  /    /::\  \       /::\  \   
     /:/\:\  \     /:/  /        /:/__/        /:/  /    /:/\:\  \     /:/\:\  \  
    /::\~\:\  \   /:/  /  ___   /::\  \ ___   /:/  /    /::\~\:\  \   /::\~\:\  \ 
   /:/\:\ \:\__\ /:/__/  /\__\ /:/\:\  /\__\ /:/__/    /:/\:\ \:\__\ /:/\:\ \:\__\
   \/__\:\ \/__/ \:\  \ /:/  / \/__\:\/:/  / \:\  \    \:\~\:\ \/__/ \/_|::\/:/  /
        \:\__\    \:\  /:/  /       \::/  /   \:\  \    \:\ \:\__\      |:|::/  / 
         \/__/     \:\/:/  /        /:/  /     \:\  \    \:\ \/__/      |:|\/__/  
                    \::/  /        /:/  /       \:\__\    \:\__\        |:|  |    
                     \/__/         \/__/         \/__/     \/__/         \|__|     

A Alive-Monitoring Package.";;

// A Entry Point Of Fuhler.
[<EntryPoint>]
let main argv = 
    printfn "%s" fuhler_logo
    let runtime_settings = getRuntimeSettings "runtime.json" in
    apiServer runtime_settings.port;
    aliveAgent.Start(); // start agent
    System.Console.WriteLine "Type Any Key To Escape...";
    let key = System.Console.ReadKey() in
    0 
