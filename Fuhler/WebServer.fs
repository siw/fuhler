﻿module Fuhler.WebServer
open System
open System.Net
open System.Web
open System.IO
open Fuhler.AliveAgent
open Fuhler.DbAccessor
open Newtonsoft.Json
open NLog

type apiResponse = {
    status : int;
    contentType : string;
    content : string;
}

let private logger = LogManager.GetCurrentClassLogger()
let private infoLog content = logger.Info<string> content;;

let private location = JsonHandler.getRuntimeSettings("runtime.json").location in
let private contentTypeResolver path =
    let ext = (Path.GetExtension path) in
    match ext with
    | ".css" -> "text/css";
    | ".js" -> "text/javascript";
    | ".html" -> "text/html";
    | _ -> "text/plain"

let private get_static_content url =
    let rp = location ^ url in
    if (File.Exists(rp)) then
        use io = new StreamReader(rp) in
        {status = 200; contentType = (contentTypeResolver url); content = io.ReadToEnd()}
    else
        {status = 404; contentType = "text/plain"; content = "Not Found"}

let private convertResultToDao (x:AliveAgentResponse) =
    let t = new healthCheckDao() in
    t.Host <- x.host;
    t.ResponseTime <- x.responseTime;
    t.Status <- x.statusCode;
    t.Time <- 0;
    t

let private executeApi url =
    infoLog ("request recieved. url=" + url)
    match url with
    | "/executeOnce" ->
        let rep = AliveAgent.aliveAgent.PostAndReply(fun rep -> Execute(new Uri("http://www.takashi-kun.com/health.txt"), rep));
        convertResultToDao rep |> DbAccessor.storeHealthCheck;
        {status = 200; contentType = "text/plain"; content = "OK"}
    | "/observe" ->
        let setting = JsonHandler.getSettings("sample.json") in
        (* AliveAgent.aliveAgent.Post Kill; *)
        let callback x = convertResultToDao x |> DbAccessor.storeHealthCheck in
        (AliveAgent.timeAgentManager callback setting.Head) |> ignore;
        {status = 200; contentType = "text/plain"; content = "OK"}
    | "/getResult" ->
        let resp = DbAccessor.getHealthCheck() in
        let arr = Seq.toArray resp in
        let js = JsonConvert.SerializeObject(arr) in
        {status = 200; contentType = "application/json"; content = js}
    | _ -> 
        get_static_content url;;

let private asyncCallBack cb = new AsyncCallback(cb) in

let rec private apiHandler (iar:IAsyncResult) =
    let ls = iar.AsyncState :?> HttpListener in
    let context = ls.EndGetContext(iar) in
    (* write a execute command *)
    let rpath = context.Request.Url.PathAndQuery in
    let result = context.Request.Url.PathAndQuery |> executeApi in
    (* set session status *)
    let buf = Text.Encoding.UTF8.GetBytes(result.content) in
    context.Response.StatusCode <- result.status;
    context.Response.ContentType <- result.contentType;
    context.Response.ContentLength64 <- buf.LongLength;
    context.Response.OutputStream.Write(buf, 0, buf.Length);

    (* close this session *)
    context.Response.Close();
    (* wait the next access *)
    let ar = asyncCallBack apiHandler in
    ls.BeginGetContext(ar, ls) |> ignore;;

let apiServer port =
   let listener = new HttpListener() in
   listener.Prefixes.Add("http://localhost:" + port.ToString() + "/");
   infoLog ("Try to Listening...");
   listener.Start();
   infoLog ("Listening Successful, port=" + port.ToString() + ".");
   listener.BeginGetContext((asyncCallBack apiHandler), listener) |> ignore;;



