﻿module Fuhler.AliveAgentTest

(* import core lib *)
open FsUnit
open FsCheck
open NUnit.Framework
open Swensen.Unquote
open Microsoft.VisualStudio.TestTools.UnitTesting
open Rhino.Mocks

(* import Fuhler *)
open Fuhler.AliveAgent
open JsonHandler

open NUnitRunner
(*
[<Test>]
let ``resToStringTest`` () = 
    let plb = new PrivateType("Fuhler", "AliveAgent") in
    let record = {isAlive = true; statusCode = 200; responseTime = 100} in
    plb.InvokeStatic("resToString", record) |> should equal "isAlive:true statusCode:200 responseTime:100"
*)


