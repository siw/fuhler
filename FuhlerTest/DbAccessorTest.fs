﻿module Fuhler.DbAccessorTest

(* import core lib *)
open FsUnit
open FsCheck
open NUnit.Framework
open Swensen.Unquote
open Microsoft.VisualStudio.TestTools.UnitTesting
open Rhino.Mocks
open System.Net

(* import Fuhler *)
open Fuhler.DbAccessor

open NUnitRunner
[<TestFixture>]
type ``DbAccessorTest``  () = 
    let test_data = new healthCheckDao() in

    [<SetUp>]
    member DbAccessorTest.``initialize`` () =
        DbAccessor.cleanUpDb();

    [<Test>]
    member DbAccessorTest.``writeValueTest`` () = 
        test_data.Host <- "test";
        test_data.Status <- 200;
        test_data.ResponseTime <- 300;
        test_data.Time <- 144;
        DbAccessor.storeHealthCheck test_data |> should equal ()
        ()

    [<Test>]
    member DbAccessorTest.``readValueTest`` () = 
        DbAccessor.storeHealthCheck (new healthCheckDao());
        DbAccessor.getHealthCheck() 
        |> Seq.length > 0
        |> should equal true







