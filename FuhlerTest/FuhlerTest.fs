﻿module FuhlerTest

// https://github.com/fsharp/FsCheck/blob/master/Docs/Documentation.md
// https://github.com/fsharp/FsUnit
// https://code.google.com/p/unquote/

(* import core lib *)
open FsUnit
open FsCheck
open NUnit.Framework
open Swensen.Unquote

(* import Fuhler *)
open Fuhler
open JsonHandler

open NUnitRunner
[<Test>]
let ``getRuntimeSettingsTest`` () =
    let wanted = {port=10000; location="../../../static"} in
    JsonHandler.getRuntimeSettings("runtime.json") |> should equal wanted

[<Test>]
let ``getSettingsTest`` () =
    let wanted = [
        {
            host = "http://yahoo.co.jp";
            duration = 1000;
            threshold_up = 2;
            threshold_down = 2;
            threshold_resp_time = 5000;
        };
        {
            host = "http://google.co.jp";
            duration = 2000;
            threshold_up = 3;
            threshold_down = 4;
            threshold_resp_time = 1000;
        }
     ] in
    JsonHandler.getSettings("sample.json") |> should equal wanted