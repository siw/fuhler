﻿module Fuhler.WebServerTest

(* import core lib *)
open FsUnit
open FsCheck
open NUnit.Framework
open Swensen.Unquote
open Microsoft.VisualStudio.TestTools.UnitTesting
open Rhino.Mocks
open System.Net

(* import Fuhler *)
open Fuhler.AliveAgent
open Fuhler.JsonHandler
open Fuhler.WebServer

open NUnitRunner
[<TestFixture>]
type ``ApiServerTest``  () = 
    let fixtureServer = apiServer 10000 in
    [<TestFixtureSetUpAttribute>]
    member ApiServerTest.``Init`` () =
        AliveAgent.aliveAgent.Start();

    [<TestFixtureTearDownAttribute>]
    member ApiServerTest.``clean`` () =
        AliveAgent.aliveAgent.Post Kill

    [<Test>]
    member ApiServerTest.``apiServerTest`` () = 
        use client = new WebClient() in
        (fun () -> client.DownloadString("http://localhost:10000") |> ignore) |> should throw typeof<WebException>

    [<Test>]
    member ApiServerTest.``executeOnceTest`` () = 
        use client = new WebClient() in
        client.DownloadString("http://localhost:10000/executeOnce") |> should equal "OK"

    [<Test>]
    member ApiServerTest.``observeTest`` () = 
        use client = new WebClient() in
        client.DownloadString("http://localhost:10000/observe") |> should equal "OK"





